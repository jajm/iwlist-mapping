# iwlist-mapping

An ifup mapping script to set the right interfaces depending on available essids

## Installation

As root:

```
cp iwlist-mapping /sbin/
```

Then edit /etc/network/interfaces and add a mapping stanza like this:

```
mapping wlan0
script iwlist-mapping
map essid1 scheme1
map essid2 scheme2

iface wlan0-scheme1 inet dhcp
wpa-conf /etc/wpa_supplicant/scheme1.conf

iface wlan0-scheme2 inet dhcp
wpa-conf /etc/wpa_supplicant/scheme2.conf
```
